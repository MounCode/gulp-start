var gulp        = require('gulp'),
    less        = require('gulp-less'),
    browserSync = require('browser-sync'),
    concat      = require('gulp-concat'),
    uglify      = require('gulp-uglifyjs'),
    cssnano     = require('gulp-cssnano'),
    rename      = require('gulp-rename');

//Компилятор less
gulp.task('less', function(){
    return gulp.src('app/less/*.less')
    .pipe(less())
    .pipe(gulp.dest('app/css'))
    .pipe(browserSync.reload({stream: true}))
});

//Собираем скрипты, объединяем.
gulp.task('scripts', function(){
    return gulp.src([
        'app/libs/jquery/dist/jquery.min.js',
        'app/libs/fancybox/dist/jquery.fancybox.min.js',
        'app/libs/owl.carousel/dist/owl.carousel.min.js'
    ])
    .pipe(concat('libs.min.js'))
    .pipe(uglify())
    .pipe(gulp.dest('app/js'));
})

//Собираем css, объединяем и минифицируем TODO
//gulp.task('css-libs',function(){
//    return gulp.src([
//        'app/libs/fancybox/dist/jquery.fancybox.min.css',
//        'app/libs/owl.carousel/dist/assets/owl.carousel.min.css'
//    ])
//    .pipe(concat('libs.min.css'))
//    .pipe(cssnano())
//    .pipe(gulp.dest('app/css'));
//})
//TODO ---------------------------------------

//Лив превью
gulp.task('browser-sync', function(){
	browserSync({
		server:{
			baseDir:'app'
		},
		notify: false
	});
});

//Следим за изменениями файлов и запускаем таски
gulp.task('watch', ['browser-sync', 'less','css-libs', 'scripts'], function() {
    gulp.watch('app/less/*.less', ['less']);
    gulp.watch('app/*.html', browserSync.reload);
    gulp.watch('app/js/**/*.js', browserSync.reload);
}); 